﻿/*
 *	Dao Danh Luu 
 * 	25/11/2015
 * 	Game Ban May Bay 2D
*/

using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	public float speed = 5;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.up * speed * Time.deltaTime);
		if (transform.position.y < -5.0f)
			Destroy (this.gameObject);
	}
}
