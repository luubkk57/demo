﻿/*
 *	Dao Danh Luu 
 * 	25/11/2015
 * 	Game Ban May Bay 2D
*/



using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	public float speed = 5;
	public GameObject dan;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey ("a")) {
			transform.Translate(Vector3.left * speed * Time.deltaTime);
		} else if (Input.GetKey ("d")) {
			transform.Translate(Vector3.right * speed * Time.deltaTime);
		}
		else if (Input.GetKey ("w")) {
			transform.Translate(Vector3.up * speed * Time.deltaTime);
		}
		else if (Input.GetKey ("s")) {
			transform.Translate(Vector3.down * speed * Time.deltaTime);
		}
		while (true) {
			if (Input.GetKeyUp (KeyCode.Space)) {
				Instantiate (dan, transform.position, Quaternion.identity);
			}
		}
	}
	void OnCollisionEnter2D(Collision2D player){
		if (player.gameObject.tag == "Enemy") {
			//Destroy(player.gameObject);
			Destroy(this.gameObject);
		}
	}
}
