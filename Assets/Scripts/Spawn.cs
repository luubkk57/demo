﻿/*
 *	Dao Danh Luu 
 * 	25/11/2015
 * 	Game Ban May Bay 2D
*/

using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour {
	public float EnemyBegin;
	public float EnemyDelay;
	public GameObject Enemy;
	// Use this for initialization
	void Start () {
		EnemyBegin = Random.Range (3f, 5f);
		EnemyDelay = Random.Range (3f, 5f);
		InvokeRepeating ("SpawnEnemy", EnemyBegin, EnemyDelay);
	}
	void SpawnEnemy () {
		Instantiate (Enemy, transform.position, transform.rotation);
	}
}
