﻿/*
 *	Dao Danh Luu 
 * 	25/11/2015
 * 	Game Ban May Bay 2D
*/

using UnityEngine;
using System.Collections;

public class danController : MonoBehaviour {
	public float speed = 10;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.up * speed * Time.deltaTime);
		if (transform.position.y > 5.3f)
			Destroy (this.gameObject);
	}
	void OnCollisionEnter2D(Collision2D coli){
		if (coli.gameObject.tag == "Enemy") {
			Destroy(coli.gameObject);
			Destroy(this.gameObject);
		}
	}

}
